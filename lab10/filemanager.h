#ifndef FILEMANAGER_H
#define FILEMANAGER_H

#include <QMainWindow>
#include <QDir>
#include <QDirModel>//каталог
#include <QInputDialog>//изменить имя
#include <QtCore>
#include <QtGui>
#include <QFileSystemModel>
#include <QMessageBox>



namespace Ui {
class FileManager;
}

class FileManager : public QMainWindow
{
    Q_OBJECT

public:
    explicit FileManager(QWidget *parent = nullptr);
    ~FileManager();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

private:
    Ui::FileManager *ui;
    QDirModel *model;
    QFileSystemModel *dirmodel;
    QFileSystemModel *filemodel;

    QString fromFile;
    QString path;
};

#endif // FILEMANAGER_H
