#include "filemanager.h"
#include "ui_filemanager.h"
#include <QMessageBox>
#include <QColorDialog>

void recDirectCopy(QString src, QString dst) {
    QDir dir(src);
    if (! dir.exists())
        return;

    foreach (QString d, dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot)) {
        QString dst_path = dst + QDir::separator() + d;
        dir.mkpath(dst_path);
        recDirectCopy(src+ QDir::separator() + d, dst_path);
    }

    foreach (QString f, dir.entryList(QDir::Files))
        QFile::copy(src + QDir::separator() + f, dst + QDir::separator() + f);
}

void recDirectDelete(QString index){
    QDir dir(index);
    if (! dir.exists())
        return;

    dir.removeRecursively();
}

FileManager::FileManager(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::FileManager),
    model(new QDirModel)
{
    ui->setupUi(this);

    model->setReadOnly (false);//отключить защиту каталога(копирование, удаление)
    model->setSorting((QDir::DirsFirst | QDir::IgnoreCase | QDir::Name));//сортировка

    ui->treeView->setModel(model);

    QModelIndex index = model->index("E://OOP lab qt//Experiment");
    ui->treeView->expand(index);//открыть каталог


    ui->treeView->scrollTo(index);//переместить к каталогу
    ui->treeView->setCurrentIndex((index));
    ui->treeView->resizeColumnToContents(0);//изменениие размера названий
}

FileManager::~FileManager()
{
    delete ui;
    delete model;
}

//кнопка создать папку
void FileManager::on_pushButton_clicked(){
    QModelIndex index = ui->treeView->currentIndex();
    if (!index.isValid()) return;

    QString name =QInputDialog::getText(this,"Новая папка","Введите название папки!");

    if (name.isEmpty()) return;

    model->mkdir(index, name);
}
//кнопка копировать
void FileManager::on_pushButton_2_clicked(){
    fromFile = model->filePath(ui->treeView->currentIndex());
}
//кнопка вставить
void FileManager::on_pushButton_4_clicked(){

    QFileInfo file(fromFile);
    QModelIndex toFile = ui->treeView->currentIndex();
    //QString fromFilePath= model->filePath(fromFile);
    QString toFilePath= model->filePath(toFile);
    if (file.isDir()){
        model->mkdir(toFile ,file.fileName());
        recDirectCopy(fromFile, toFilePath + '/' + file.fileName() + '/');
    }
    else {
        QFile::copy(fromFile, toFilePath + '/' + file.fileName());
    }
    model->refresh();
}
//кнопка удалить
void FileManager::on_pushButton_5_clicked(){
    QModelIndex index = ui->treeView->currentIndex();
    if (!index.isValid()) return;
    if (model->fileInfo(index).isDir()){
        QDir dir(model->filePath(index));
        dir.removeRecursively();
    }
     else
        model->remove(index);
    model->refresh();
}


